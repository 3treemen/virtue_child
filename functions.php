<?php
// Add custom functions here
// 
add_filter( 'woocommerce_variation_is_active', 'grey_out_variations_when_out_of_stock', 10, 2 );
function grey_out_variations_when_out_of_stock( $grey_out, $variation ){
if ( ! $variation->is_in_stock() ){
  return false;
 }else{
  return true;
 }
}



add_action( 'woocommerce_before_shop_loop_item_title', function() {
 ?><div class="woo-thumbnail-wrap"><?php
}, 9 );
add_action( 'woocommerce_before_shop_loop_item_title', function() {
 ?></div><?php
}, 11 );